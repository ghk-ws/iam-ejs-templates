# Domino IAM OAuth Templates

This repo serves the Domino IAM EJS templates as standalone files.

In that way it's easy to have a look at them and style them without the need of a whole Domino-Proton-IAM setup.

## Install

`npm install`

## Run

`npm start`

Goto [http://localhost:7001](http://localhost:7001)

The start screen is just for navigating to the different templates.
