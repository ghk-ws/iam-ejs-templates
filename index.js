const Koa = require('koa');
const render = require('koa-ejs');
const path = require('path');

const app = new Koa();
const root = path.join(__dirname, 'views');
const opts = {};
app.use(require('koa-static')(root, opts));

render(app, {
  root: path.join(__dirname, 'views'),
  layout: '_layout',
  viewExt: 'ejs',
  cache: false,
  debug: false,
});

app.use((ctx, next) => {
  ctx.state = ctx.state || {};
  ctx.state.now = new Date();
  ctx.state.ip = ctx.ip;
  ctx.state.version = '2.0.0';
  return next();
});

app.use(async (ctx) => {
  const {
    request: { url },
  } = ctx;
  let view = '';
  switch (url) {
    case '/error':
      view = 'error';
      break;
    case '/interaction':
      view = 'interaction';
      break;
    case '/logout':
      view = 'logout';
      break;
    case '/login':
      view = 'login';
      break;
    case '/login_ghk':
      view = 'login_ghk';
      break;
    case '/':
    default:
      view = null;
      break;
  }

  if (!view) {
    ctx.type = 'html';
    ctx.body = '<a href="/login">Login</a><br/>';
    ctx.body += '<a href="/logout">Logout</a><br/>';
    ctx.body += '<a href="/interaction">Interaction</a><br/>';
    ctx.body += '<a href="/error">Error</a><br/>';

    ctx.body += '<br/><a href="/login_ghk">Login GHK domcfg.nsf</a><br/>';
  } else {
    await ctx.render(view, {
      title: 'Test',
      uuid: '123-456-789-0',
      rejectUrl: 'http://reject.this',
      client: {
        clientName: 'TestClient',
      },
      scopes: [
        {
          id: '12345',
          name: 'Test Scope',
          grantMessage:
            'Die Anwendung erhält grundlegende Information über Ihr Konto, wie ihre ID, Ihren Namen und Ihre E-Mail Adresse.',
        },
      ],
      // only for error
      error: {
        error: 'Total Error',
        error_description: 'Worst case has happened',
      },
      // only for logout
      form: {},
      // only for interaction
      account: {
        email: 'user@test.com',
      },
    });
  }
});

if (process.env.NODE_ENV === 'test') {
  module.exports = app.callback();
} else {
  app.listen(7001);
  console.log('open http://localhost:7001');
}

app.on('error', (err) => {
  console.log(err.stack);
});
